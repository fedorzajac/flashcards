#miscrosoct dev api onedrive
# generated app secret:  ohpxYFQCOP155$gibQ92[(@
#app name: My Ruby App
#App id or client id: 0f80a942-5622-4a33-bb01-0c4d1cfaa60e

class App

	attr_reader :options

	def initialize(arguments, stdin)
		@arguments = arguments
		@stdin = stdin

		@options = nil
	end

	def run
		puts "hello"
		p @arguments
		#slurp(@arguments[0])
		out = get("select * from JMDict_e where kanji like '%亀%' ")
		out.each do |row|
			p row.join(" - ")
		end
	end

	protected

	def hello
	end

	def slurp(file)
		system("sqlite3 jisho.sqlite3 <<!
.echo on
.headers on
.mode tabs
.import #{file} #{File.basename(file,'.tsv')}
!
		")
	end

	def get(sql)
		out = `sqlite3 -header jisho.sqlite3 "#{sql}"`
		out.split("\n").map!{|row| row.split("|")}
	end

end

app = App.new(ARGV,STDIN)
app.run
