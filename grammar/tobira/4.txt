毎〜のように		skoro každý ... robíme ...	映画が好きなので、毎週のように映画を見に行きます。；子どもの頃は、毎年のように夏休みに海に行っていた。；このサイトにアクセスすると、毎回のようにフリーズしてしまう。
Sentence と考えられている／思われている	used to introduce generally accepted opinion		日本人は一般的には丁寧だと言われている。；日本食べ物は体にいいと考えられているが、実は、天ぷらやトンカツなど、油をたくさん使うカロリーの高い料理も多い。；将来は、宇宙にも人間が住めるようになるだろうと考えられている。
Sentence と考えられる／思われる	used to introduce speakers opinion
〜などは／なんて	things/people/etc. like ~
まず	first of all; first; to begin with; before everithing		朝、起きたら、私はまずコーヒーを飲む。
Verb-masu 合う	V (to/for) each other; V with	navzájom; vzájomne; medzi sebou
Verb-non-past ように	state or event beyond the control of a subject; tameni - in control of a subject	tameni - nutná podmienka	日本語が上手になるように、毎日練習している。；先生は、学生がわかるように優しい単語を使って説明した。朝寝坊をしないように、目覚まし時計をセットしておきます。
ある　Noun			昔、昔、ある所に、…
Sentence ｛の／ん｝ ｛ではないだろうか／ではないでしょうか／じゃないかな｝	used to express an opinion in an indecisive fashion. no negative meaning.		地球温暖化問題はもっと大きくなっていくのではないでしょうか。；あのクラスは、毎日宿題やテストがあるので、大変なのではないだろうか。
〜ず（に）	without V-ing; instead of V-ing		辞書を見ずに新聞が読めるようになりたいです。；両親に相談せずに、留学することを決めてしまった。
｛そう／こう／ああ｝ いうNoun	that; this; kind of; such
Sentenceと言える｛だろう／でしょう｝	speaker is quite certain that his statement is correct but wants to soften the statement		奈良は日本で一番歴史の古い町の一つと言えるでしょう。
XはY（という）ことなの｛である／た｝	X is Y; X means that Y
〜ん｛だけど／ですが｝	preliminary remark to inform the hearer of the speaker's current situation; but		大阪まで新幹線で行きたいんですが、いくらでしょうか。
それで	because of that; so; for that reason
Question word 〜ても	no matter what/who/when/where/how		世界中、どこに行っても、マクドナルドが食べられる。
〜うちに	do something before state/situation changes		熱いうちに、召し上がって下さい。；暑くならないうちに、犬の散歩をしてきた方がいいよ。
できれば；できたら	if possible; if you don't mind;
〜たばかり	something just happened		今、食べたばかりですから、お腹がいっぱいで、何も食べられません。
