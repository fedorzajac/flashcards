# encoding: UTF-8

require 'sinatra'
#require 'rack/ssl'
require 'httpclient'
require 'json'
require 'base64'
# require_relative './lib/test_jp_sk'
require_relative './lib/DictJP'
require_relative './routes/lookup'
require_relative './routes/jisho'
require_relative './lib/GoogleSheets'

set :set_name, 'default'

#use Rack::SSL
puts `ifconfig | grep inet`

helpers do
  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? and @auth.basic? and @auth.credentials and @auth.credentials == ['fedor', 'fedor']
  end
end

before do
  protected!
end

get '/' do
  erb :index, :layout => false
end

get 'test' do
end

get '/mini/:id' do
  #erb :mini, :layout => false
  @data = File.readlines("./vocab/tobira/tobira_#{params[:id]}.tab",:encoding => 'UTF-8')
  @data.map!{ |row| r = row.chomp.gsub(/[\t]+/,"\t").split("\t"); {k:r[0], r:r[1], t:r[2]} }
  erb :mini, :layout => false
end

get '/flip/custom/:id' do
  @data = File.readlines("./vocab/custom/#{params[:id]}.tab",:encoding => 'UTF-8')
  @data.map!{ |row| r = row.chomp.split("\t"); {k:r[0], r:r[1], t:r[2]} }
  @data.shuffle!
  erb :flip, :layout => false;
end

get '/flip/kanji/:id' do
  @data = JSON.parse(File.read("./vocab/kanji/#{params[:id]}.json",:encoding => 'UTF-8')).shuffle
  # p @data
  erb :flip_kanji, :layout => false;
end

get '/flip/:set/:id' do
  @data = File.readlines("./vocab/#{params[:set]}/#{params[:set]}_#{params[:id]}.tab",:encoding => 'UTF-8')
  @data.map!{ |row| r = row.chomp.split("\t"); {k:r[0], r:r[1], t:r[2]} }
  @data.shuffle!
  @link = "/vocab/#{params[:set]}/#{params[:set]}_#{params[:id]}.tab"
  erb :flip, :layout => false;
end

get '/show/:file' do
  @data = File.readlines(Base64.decode64(params[:file]),:encoding => 'UTF-8')
  @data.map!{ |row| r = row.chomp.split("\t"); {k:r[0], r:r[1], t:r[2]} }
  erb :show
end

get '/learn/:file' do
  @data = File.readlines(Base64.decode64(params[:file]),:encoding => 'UTF-8')
  @data.map!{ |row| r = row.chomp.split("\t"); {k:r[0], r:r[1], t:r[2]} }
  @data.each do |row|
    p row
    row[:k].split('').each do |k|
      p `sqlite3 vocab/tangorin/db.sqlite3 "select * from general where kanji='#{k}'"`
    end
  end
  erb :learn, :layout => false;
end
# get '/show/:set/:id' do
#   @data = File.readlines("./vocab/#{params[:set]}/#{params[:set]}_#{params[:id]}.txt",:encoding => 'UTF-8')
#   @data.map!{|row| r = row.chomp.split("\t"); {k:r[0], r:r[1], t:r[2]} }
#   erb :show
# end

get '/grammar/:file' do
  @data = File.readlines('./grammar/tobira/' + Base64.decode64(params[:file]),:encoding => 'UTF-8')
  @data.map!{ |row| r = row.chomp.split("\t"); {k:r[0], r:r[1], t:r[2], ex:r[3]} }
  @data.each do |row|
    p row
    p row[:k].length
  end
  @data.shuffle!
  erb :flip_gr, :layout => false;
end

get '/jisho2' do
  erb :jisho_full
end

post '/jisho2' do
  if params[:word] then
    dict = FullDict.new
    @h = dict.search(params[:word])
  end
  erb :jisho_full
end

get '/gr/:id' do
  @src = "/tobira_gr/7/#{params[:id]}.png"
  erb :gr
end

post '/jisho2' do
  if params[:word] then
    response = HTTPClient.get URI::encode("http://jisho.org/api/v1/search/words?keyword=#{params[:word]}", :follow_redirect => true)
    @h = JSON.parse(response.body)
  end
  erb :jisho_full
end

get '/sample' do
  file = './sample.txt'
  if File.exist?(file) then
    file = File.readlines(file)
    '<table>'
      file.each { |line| e = line.split("\t"); "\t<tr><td>#{e[0]}</td><td>#{e[1]}</td><td>#{e[2]}</td></tr>\n" }
    '</table>'
  end
end

get '/all' do
  # content_type 'text/plain'
  a = []
  a << "<table>"
  files = Dir.glob('./vocab/*/*.{tab,txt}')
  files.each do |file|
    File.readlines(file, :encoding => 'UTF-8').each do |line|
      e = line.split("\t")
      a << "<tr><td>#{file}</td><td>#{e[0]}</td><td>#{e[1]}</td><td>#{e[2]}</td></tr>\n"
    end
  end
  a << "</table>"
  a.join()
end

get '/sets' do
  # content_type 'text/plain'
  a = []
  a << "<table>"
  files = Dir.glob('./vocab/custom/sets.tab')
  files.each do |file|
    File.readlines(file, :encoding => 'UTF-8').each do |line|
      e = line.split("\t")
      a << "<tr><td>#{e[0]}</td><td>#{e[1]}</td><td>#{e[2]}</td><td>#{e[3]}</td><td>#{e[4]}</td></tr>\n"
    end
  end
  a << "</table>"
  a.join()
end