require './lib/GoogleSheets'

describe 'GoogleSheets' do
  # it 'retrieves content' do
  #   sh = GoogleSheets.new(scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS_READONLY)
  #   sh.all_data(spreadsheet_id: '1YoNEiSRUV5X2tJ8jXZ8fen06h1KCyXdBJKEqjCNB7SU', range: 'ch1-1!A:C') { |row| 
  #     expect(row[0]).to eq "地理"
  #     break
  #   }
  # end

  it 'writes content' do
    sh = GoogleSheets.new(scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    data = [[1,2,3]]
    expect(sh.write_data(spreadsheet_id: '1MeDfl_EyqpVbwRvou-7EOJjwCyGjkUtlcb0iwHTA0t8', values: data)).to eq data.flatten.count
  end

end