get '/new_set' do
  settings.set_name = params[:set_name]
  redirect '/jisho'
end

get '/jisho' do
  file = "./vocab/custom/#{Time.now.strftime("%F")}.tab"
  @set = File.readlines(file, encoding: 'UTF-8') if File.exists?(file)
  erb :jisho
end

post '/jisho' do
  file = "./vocab/custom/#{Time.now.strftime("%F")}.tab"
  @set = File.readlines(file, encoding: 'UTF-8') if File.exists?(file)
  if params[:word] then
    # response = HTTPClient.new.get(URI::encode("http://jisho.org/api/v1/search/words?keyword=#{params[:word]}"))
    # @h = JSON.parse(response.body)
    dict = DictJP.new
    @h = dict.search(params[:word])
  end
  erb :jisho
end

post '/save' do
  # row = "#{params[:word].chomp}\t#{params[:reading].chomp}\t#{params[:translation].chomp}"#\t#{params[:set_name].chomp}"
  data = [[params[:word].chomp, params[:reading].chomp, params[:translation].chomp, Time.now.strftime("%F"), settings.set_name]]
  row = data[0].join("\t")
  p row
  # p row[0..-2]
  # File.open("./vocab/custom/#{Time.now.strftime("%F")}.tab",'a+', encoding: 'UTF-8') do |f|
  #   f.puts row
  # end
  File.open("./vocab/custom/sets.tab",'a+', encoding: 'UTF-8') do |f|
    f.puts row
  end
  # sh = GoogleSheets.new(scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
  # sh.write_data(spreadsheet_id: '1MeDfl_EyqpVbwRvou-7EOJjwCyGjkUtlcb0iwHTA0t8', values: data)

  redirect '/jisho'
end