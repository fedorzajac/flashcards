require_relative 'main'
require 'test/unit'
require 'rack/test'

class Test_it_runs < Test::Unit::TestCase
  include Rack::Test::Methods
    def app
      Sinatra::Application
    end

    def test_it_runs_main_page
      get '/'
      assert last_response.ok?
      assert_equal true, last_response.body.include?('Jisho')
    end
end