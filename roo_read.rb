require 'roo'

class String
	def black;          "\033[30m#{self}\033[0m" end
	def red;            "\033[31m#{self}\033[0m" end
	def green;          "\033[32m#{self}\033[0m" end
	def brown;          "\033[33m#{self}\033[0m" end
	def blue;           "\033[34m#{self}\033[0m" end
	def magenta;        "\033[35m#{self}\033[0m" end
	def cyan;           "\033[36m#{self}\033[0m" end
	def gray;           "\033[37m#{self}\033[0m" end
end

def get(sql)
	out = `sqlite3 -header jisho.sqlite3 "#{sql}"`
	out.split("\n").map!{|row| row.split("|")}
end

# Load an encrypted OpenOffice Spreadsheet
#ods = Roo::OpenOffice.new("myspreadsheet.ods", password: "password")
def terminal
	ods = Roo::OpenOffice.new("./niponica.ods")

	puts ods.sheets
	puts ods.sheet(0).row(1)

	ods.sheet(0).each do |row|

		#p ods.sheet(0).cell(4,2) #row,column
		puts row[1]
		#words = ods.sheet(0).cell(4,4).split("、")
		#words = row[3].nil? ? [""] : row[3].split("、")
		if !row[3].nil? then
			words = row[3].split("、")
			words.each do |word|
				#p word
				help = get("select kanji,reading,meaning,note from JMDict_e where kanji like '#{word}'").uniq
				skip = true
				help.each do |row|
					if skip then
						skip = false
						next
					end
					row[0] = row[0].red
					row[2] = row[2].green
					#puts row.join(',')
					puts "\t#{row[0]}, #{row[1]}"
					puts "\t\t#{row[2]}"
				end
			end
		end

	end
end

def arr_hsh

	data = [{}]

	ods = Roo::OpenOffice.new("./niponica.ods")

	ods.sheet(0).each do |row|

		#p ods.sheet(0).cell(4,2) #row,column
		#puts row[1]
		temp = []
		#words = ods.sheet(0).cell(4,4).split("、")
		#words = row[3].nil? ? [""] : row[3].split("、")
		if !row[3].nil? then
			words = row[3].split("、")
			words.each do |word|
				#p word
				help = get("select kanji,reading,meaning,note from JMDict_e where kanji like '#{word}'").uniq
				skip = true
				help.each do |row|
					if skip then
						skip = false
						next
					end
					#row[0] = row[0].red
					#row[2] = row[2].green
					#puts row.join(',')
					#puts "\t#{row[0]}, #{row[1]}"
					#puts "\t\t#{row[2]}"
					temp << {word:row[0],reading:row[1],translation:row[2]}
				end
			end
		end
		data << {sentence:row[1],data:temp} if !row[1].nil?
	end
	data
end

def run_hash
	text = arr_hsh

	10.times do |i|
		puts text[i][:sentence]
		if !text[i][:data].nil? then
			text[i][:data].each do |data|
				data.keys.each do |key|
					puts "#{key} - #{data[key]}"
				end
			end
		end
	end
end

#terminal


def test_1
	str = "客はこれに茶碗を乗せて茶を飲んだり、茶碗を拝見したりする"
	out = `sqlite3 jisho.sqlite3 "select distinct kanji from JMDict_e"`
	words = out.split("\n")
	words.each do |word|
		p word if str.include? word
	end

end

test_1
