
#response = HTTPClient.new.get(URI::encode("http://tangorin.com/kanji/字"), :follow_redirect => true)
#p response


#doc = Nokogiri::HTML(open(URI::encode("http://tangorin.com/kanji/字")))
#ap doc.xpath("//div[@class='k-sod']")

threads = []

i=0

data = (ARGV.empty? ? DATA : ARGF).readlines.map{|e| e.chomp}

#require_relative 'kanji'

data = (HIRAGANA+KATAKANA+GRADE_1+GRADE_2+GRADE_3+GRADE_4+GRADE_5+GRADE_6+JUNIOR_HIGH_SCHOOL+JINMEIYOU).gsub(" ","").split("")
puts data.count

tan = TangorinDictJP.new
data.each do |kanji|
	threads << Thread.new do
		puts "#{i}, #{kanji}"
		i+=1
		svg = tan.getDrawingDiagramFor(kanji)
		File.open("vocab/strokes/" + kanji + ".html","w+") do |file|
			file.puts svg
		end
	end
	sleep(0.1)
end

threads.each {|t| t.join}

__END__
奢
