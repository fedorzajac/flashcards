require 'json'
require 'uri'
require 'Nokogiri'
require 'open-uri'

class Dict
  def initialize
  end

  def search(word)
    doc = Nokogiri::HTML(open("https://slovnik.azet.sk/preklad/slovensko-anglicky/?q=#{word}"))
    temp = nil
    h = Hash.new { |hash, key| hash[key] = [] }
    # a = doc.at_css('[class="p"]').inner_html
    doc.xpath("//tr").each do |row|
      begin
        f = row.text.split(" → ")
        temp = f[0] if f[0] != ""
        h[temp] << f[1][0...f[1].length/2] if f[1][0] != "\n"
        # p temp
        # p f
        # puts "#{temp}\t#{f[1][0...f[1].length/2]}"
        # p "---"
      rescue
      end
    end
    h
  end
end
