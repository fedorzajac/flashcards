
require_relative './TangorinDictJP'
require_relative './kanji'

#data = (HIRAGANA+KATAKANA).gsub(" ","").split("")

include_keystrokes = false
words = File.readlines(ARGV[0])
words.map!{|row| r = row.chomp.split("\t"); {:k => r[0], :r => r[1], :t => r[2]}}

#p words



tan = TangorinDictJP.new


kana = (HIRAGANA+KATAKANA+"〜々").gsub(" ","").split("")


puts "
<html>
<head>
	<meta charset='utf-8' />
	<style>
		table {
			# page-break-inside:auto;
			border-collapse: collapse;
			border: 1pt solid #999;
		}
		tr {
			page-break-inside:avoid;
			page-break-after:auto;
		}
		td {
			border:1pt solid #999;
			height:10mm;
		}
		td.mm70 {
			width: 70mm;
		}
		td.mm50 {
			width: 50mm;
		}
	</style>
</head>
<body>"

	puts "<table>"
	words.shuffle.each_with_index do |row,i|
		puts "<tr>"
		puts "<td class=mm50></td>"
		puts "<td class=mm50>#{row[:k]=="" ? row[:r] : row[:k] }</td>"
		puts "<td class=mm50></td>"
		puts "<td class=mm50>#{row[:t]}</td>"
		puts "</tr>"
		#puts "</table>lalalala<table>" if i+1%15 == 0
		if include_keystrokes then
			row[:k].split("").each do |k|
				#puts k
				next if kana.include? k
				begin
					svg = IO.read("./vocab/strokes/#{k}.html")
				rescue
					svg = tan.getDrawingDiagramFor(k)
				ensure
					svg = svg.gsub('<line x1="55" y1="0" x2="55" y2="109" style="stroke:#777777; stroke-width:0.5; stroke-dasharray: 4, 4;"></line>',"")
					.gsub('<line x1="0" y1="55" x2="109" y2="55" style="stroke:#777777; stroke-width:0.5; stroke-dasharray: 4, 4;"></line>',"")
					.gsub('fill:none;stroke:#333333;','fill:none;stroke:#000;')
					.gsub('stroke-width:4;"></path>','stroke-width:3;"></path>')
					.gsub('width="52px" height="52px" viewbox="0 0 109 109"','width="5mm" height="5mm" viewbox="0 0 109 109"')
					.gsub("<div class=\"k-sod\">","").gsub("</div>","")
				end
				puts "<tr><td colspan=3>#{svg}</td></tr>"
			end
		end
	end
	puts "</table>"

puts "</body></html>"
