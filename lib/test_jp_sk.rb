require_relative './DictJP'
require_relative './Dict'
require 'awesome_print'

class FullDict
	def initialize
	end

	def search(word)
		tr = DictJP.new
		sk = Dict.new

		#word = "擬声語"

		h = tr.search(word)

		ret = []

		if h["data"] then
			h["data"].each do |arr|
				a = arr["japanese"][0]["word"]
				b = arr["japanese"][0]["reading"]
				c = arr["senses"][0]["english_definitions"].join("; ")
				#puts "#{a}\t#{b}\t#{c}"
				#ap "---------------------"
				arr["senses"][0]["english_definitions"].each do |en_def|
					sk_h = sk.search(en_def.gsub(/\([^()]*\)/, ''))
					#sk_h = sk.search(en_def.gsub(/\([^()]*\)/, ''))
					sk_h.keys.each do |en_key|
						#printf "%s ".red % a
						#printf "%s ".yellow % b
						#printf "%s ".green % c
						#printf "%s ".blue % en_key
						#puts h[en_key].join("; ")
						ret << {
							:jw => a,
							:jr => b,
							:je => c,
							:en_key => en_key,
							:sk_key => h[en_key].join("; ")
						}
					end
				end
			end
		end
		ret
	end

end

#dict = FullDict.new
#p dict.search("座席")]

__END__


くだけた
実は
複雑
決まり
課
丁寧な
使い分ける
言語
相手
変える
部分
男性
女性
比べる
文末
表れる
彼の方
あいつ
感じ
多くの
場面
観察
表
男女
俺
この辺に
おごる
随分
文字
恋人
例
表現
省略
短縮系
最後
連絡
はっきり
誘う
断る
都合が悪い
気分
お願い
大切にする
話し言葉
簡単な
こういう
慣れる
倒置
理由
状況
このような
特徴
書き言葉
必要
携帯電話
普通
体
論文
興味深い
会う
