# require 'rest-client'
# require 'excon'

class DictJP
  def initialize
  end

  def search(word)
    response = HTTPClient.new.get(URI::encode("http://jisho.org/api/v1/search/words?keyword=#{word}"), :follow_redirect => true)
    #p response
    JSON.parse(response.body)
  end

  def search2(word)
    ret = []
    h = self.search(word)
    if h["data"] then
      h["data"].each do |arr|
        a = arr["japanese"][0]["word"]
        b = arr["japanese"][0]["reading"]
        c = arr["senses"][0]["english_definitions"].join("; ")
        ret << {:word => a, :reading => b, :english_definitions => c}
      end
    end
    ret
  end

  def test(input,full=false)
    #input = (ARGV.empty? ? DATA : ARGF).readlines.map{|e| e.chomp}
    #p input.split("\n")
    input.each do |word|
      #puts "--------#{word}-------"
      h = self.search(word)
      if h["data"] then
        h["data"].each do |arr|
          a = arr["japanese"][0]["word"]
          b = arr["japanese"][0]["reading"]
          c = arr["senses"][0]["english_definitions"].join("; ")
          puts "#{a}\t#{b}\t#{c}"
          break if !full
        end
      end
    end
  end

end

__END__

差
交差
座席
固体
倉庫
後悔
個人
言語
検査
稽古
相互
砂漠
誤解
以後
前後
即座
冷蔵庫
呼吸
後輩
差別
調査
語学
正午
左右
砂糖
今後
車庫
今後
左右
金庫
古典
誤解
砂漠
個人
差別
交差
冷蔵庫
後悔
中古
以後
座席
固体
検査
呼吸
砂糖
正午
相互
倉庫
言語
調査
語学
原始
指示
四捨五入
不思議
刺激
必死
四角
支配
紙幣
資源
防止
指定席
名刺
支配
試合
表紙
使用
電子
四季
雑誌
歴史
中止
調子
支出
資格
#VALUE!
資料
教師
市場
表紙
名刺
防止
私立
資本
四角
禁止
支出
子孫
不思議
死体
必死
支払
支配
刺激
開始
調子
思想
資格
指定
支給
帽子
名字
組織
多少
無事
治療
図表
祖父
幼児
他人
寺院
事態
維持
自治
掲示
用事
事情
数字
自慢
事実
自身
自動
政治
自然
事件
自殺
他人
文字
自信
地図
治安
先祖
政治
食事
育児
指示
持参
自殺
自然
辞書
図鑑
事情
無事
治療
多少
用事
時速
自由
自動
臨時
自宅
電池
用途
努力
態度
電波
承知
首都
知能
地味
価値
破片
知恵
都会
地方
地点
知人
湿度
地球
制度
地下室
毎度
地盤
度忘
角度
位置
速度
位置
途中
遅刻
地球
高度
地方
電池
地面
知人
角度
破産
態度
努力
承知
知識
速度
装置
地味
電波
土地
地震
地下
毎度
知恵
付属
美容
不潔
皮膚
被害
比較
是非
恐怖
否定
消費
設備
不平不満
普段
分布
付近
予備
美人
婦人
勝負
不自由
非常
夫婦
警備
不幸
準備
設備
比較的
部族
符号
不思議
予備
普段
不規則
美人
悲劇
美容
非常
勝負
不通
普通
被害
皮膚
消費
非難
夫婦
費用
不便
否定
不満
飛行
