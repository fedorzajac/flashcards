data = File.readlines(ARGV[0])

puts "
<html>
	<head>
		<style>
			td {
				height:10mm;
				width:10mm;
				border:1px solid #999;
			}
			td.jap {
				height:50mm;
				width:10mm;
				border:1px solid #999;
				writing-mode:vertival-rl;
				text-align:center;
				vertical-align:top;

			}
			table {
				border-collapse:collapse;
			}
		</style>
	</head>
	<body>"

puts "<table>"
puts "<tr>"

data.reverse.map do |row|
	a,b,c = row.split("\t")
	puts "<td class=\"jap\">#{a}</td>"
end
puts "</tr>"
	15.times do
		puts "<tr>"
		data.count.times {puts "<td></td>"}
		puts "</tr>"
	end

puts "</table>"

puts "
	</body>
</html>"
