require 'google/apis/sheets_v4'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'

class GoogleSheets

  @service = nil
  OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'
  APPLICATION_NAME = 'Google Sheets API Ruby Quickstart'
  CLIENT_SECRETS_PATH = './lib/client_secret.json'
  CREDENTIALS_PATH = File.join(Dir.home, '.credentials',  "sheets.googleapis.com-ruby-quickstart.yaml")
  SCOPE = Google::Apis::SheetsV4::AUTH_SPREADSHEETS_READONLY

  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials

  def authorize(scope: SCOPE)
    FileUtils.mkdir_p(File.dirname(CREDENTIALS_PATH))
    client_id = Google::Auth::ClientId.from_file(CLIENT_SECRETS_PATH)
    token_store = Google::Auth::Stores::FileTokenStore.new(file: CREDENTIALS_PATH)
    authorizer = Google::Auth::UserAuthorizer.new(client_id, scope, token_store)
    user_id = 'default'
    credentials = authorizer.get_credentials(user_id)
    if credentials.nil?
      url = authorizer.get_authorization_url(
      base_url: OOB_URI)
      puts "Open the following URL in the browser and enter the " +
        "resulting code after authorization"
      puts url
      code = gets
      credentials = authorizer.get_and_store_credentials_from_code(
      user_id: user_id, code: code, base_url: OOB_URI)
    end
    credentials
  end

  def initialize(scope: SCOPE)
  # Initialize the API
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.client_options.application_name = APPLICATION_NAME
    @service.authorization = authorize(scope: scope)
  end

  def all_data(spreadsheet_id: '', range: '')
    response = @service.get_spreadsheet_values(spreadsheet_id, range)
    #puts 'Name, Major:'
    puts 'No data found.' if response.values.empty?
    if block_given?
      response.values.each do |row|
        # Print columns A and E, which correspond to indices 0 and 4.
        # puts "%-10s | %-10s | %-10s" % row
        yield row
      end
    else
      data = []
      response.values.each do |row|
        data << row
      end
      return data
    end
  end
  
  def write_data(spreadsheet_id: '', range: 'A1', values: nil)
    # values = [[4,5,6],[7,8,9],[r.rand(100),r.rand(100),r.rand(100)]]
    value_range = Google::Apis::SheetsV4::ValueRange.new(values: values)
    result = @service.append_spreadsheet_value(spreadsheet_id, range, value_range, value_input_option: 'RAW')
    result.updates.updated_cells
  end
end

