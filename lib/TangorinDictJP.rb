#require 'rest-client'
# require 'excon'
require 'httpclient'
require 'json'
require 'Nokogiri'
#require 'awesome_print'
require 'thread'


class TangorinDictJP
	def initialize
	end

	def search(word)
		response = HTTPClient.new.get(URI::encode("http://tangorin.com/kanji/字"), :follow_redirect => true)
		#p response
		JSON.parse(response.body)
	end

	def getDrawingDiagramFor(kanji)
		doc = Nokogiri::HTML(open(URI::encode("http://tangorin.com/kanji/#{kanji}")))
		doc.xpath("//div[@class='k-sod']").inner_html
	end

	def search2(word)
		ret = []
		h = self.search(word)
		if h["data"] then
			h["data"].each do |arr|
				a = arr["japanese"][0]["word"]
				b = arr["japanese"][0]["reading"]
				c = arr["senses"][0]["english_definitions"].join("; ")
				ret << {:word => a, :reading => b, :english_definitions => c}
			end
		end
		ret
	end

	def test(input)

		#input = (ARGV.empty? ? DATA : ARGF).readlines.map{|e| e.chomp}

		#p input.split("\n")
		input.each do |word|
			#puts "--------#{word}-------"
			h = self.search(word)
			if h["data"] then
				h["data"].each do |arr|
					a = arr["japanese"][0]["word"]
					b = arr["japanese"][0]["reading"]
					c = arr["senses"][0]["english_definitions"].join("; ")
					puts "#{a}\t#{b}\t#{c}"
				end
			end
		end
	end
end
