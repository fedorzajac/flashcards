data = File.readlines(ARGV[0])

puts "
<html>
	<head>
		<style>
		@media print {
			table {
				page-break-after: always;
			}
		}
			td {
				height:10mm;
				width:10mm;
				border:1px solid #999;
			}
			td.jap {
				height:10mm;
				width:50mm;
				border:1px solid #999;
			}
			table {
				border-collapse:collapse;
			}
		</style>
	</head>
	<body>"

puts "<table>"

i = 0

data.map do |row|
	a,b,c = row.split("\t")
	if a != "" then
		puts "<tr><td class=\"jap\">#{a}</td>"
		15.times {puts "<td></td>"}
		puts "</tr>"
		i+=1
	end

	if i >= 23 then
		puts "</table><table>"
		i = 0
	end
end

puts "</table>"

puts "
	</body>
</html>"
