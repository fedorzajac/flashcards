	キャラクター	character; personality; disposition
	ポップ	pop
広まる	ひろまる	to spread; to be propagated
様々	さまざま	varied; various
経済	けいざい	economics; business; finance; economy
影響	えいきょう	influence; effect
元	もと	origin; source
	ストーリー	story
翻訳	ほんやく	translation
読者	どくしゃ	reader
原作	げんさく	original work
増やす	ふやす	to increase; to add to; to augment
	コンテンツ	contents; content
欧米	おうべい	Europe and America; the West
少年	しょうねん	boy; juvenile; young boy; youth; lad
週刊誌	しゅうかんし	weekly publication; weekly magazine
少女	しょうじょ	little girl; maiden; young lady; female usually between 7 and 18 years old
出版	しゅっぱん	publication
捲る	めくる	to turn over; to turn pages of a book
開く	ひらく	to open; to undo; to unseal; to unpack
魅力	みりょく	charm; fascination; glamour; glamor; attraction; appeal
思い浮かぶ	おもいうかぶ	to occur to; to remind of; to come to mind
方法	ほうほう	method; process; manner; way; means; technique
確立	かくりつ	establishment; settlement
第二次世界大戦	だいにじせかいたいせん	Second World War; World War II; WWII; WW2
医学	いがく	medical science; medicine
博士	はかせ	expert; learned person
家	いえ	house; residence; dwelling
亡くなる	なくなる	to die
作品	さくひん	work (e.g. book, film, composition, etc.); opus; performance; production
本名	ほんみょう	real name
虫	むし	insect; bug; cricket; moth; worm
ベレー帽	ベレーぼう	beret
丸い	まるい	round; circular; spherical
鼻	はな	nose
人物	じんぶつ	person; character; figure; personage; man; woman
ＳＦ	エスエフ	science fiction; sci-fi; SF
年代	ねんだい	age; era; period; date
切っ掛け	きっかけ	chance; start; cue; excuse; motive; impetus; occasion
放送	ほうそう	broadcast; broadcasting
哲学	てつがく	philosophy
芸術	げいじゅつ	(fine) art; the arts
宇宙	うちゅう	universe; cosmos; space
	テーマ	theme; topic; subject matter; motif; project; slogan
理論	りろん	theory
命	いのち	life; life force
無意	むい	unintentional
人類	じんるい	mankind; humanity
未来	みらい	the future (usually distant)
	オノマトペ	onomatopoeia
深い	ふかい	deep
素晴らしい	すばらしい	wonderful; splendid; magnificent
後	あと	behind; rear
夢見る	ゆめみる	to dream (of)
世の中	よのなか	society; the world; the times
世代	せだい	generation; the world; the age
愛情	あいじょう	love; affection
越える	こえる	to cross over; to cross; to pass through; to pass over (out of); to go beyond; to go past
才能	さいのう	talent; ability
気持ち	きもち	feeling; sensation; mood
元々	もともと	originally; by nature; from the start
